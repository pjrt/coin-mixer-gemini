{-# LANGUAGE OverloadedStrings, FlexibleContexts #-}
module Main where

import Control.Concurrent
import Control.Exception
import Control.Monad (forever, void)
import Control.Monad.IO.Class (liftIO)
import Control.Flow (takeIn, doleOutDeposit)

import Data.CoinMixer
import Data.Error
import Data.Text (Text, unpack)
import Data.Text.Encoding (decodeUtf8)
import Database.Redis (Connection, defaultConnectInfo, connect)

import JobCoin (Addr)
import Network.HTTP.Client (newManager, defaultManagerSettings)

import qualified Database.Redis as R

type Worker = Addr -> CoinMixer ()

main :: IO ()
main = do
    conn <- connect defaultConnectInfo
    manager <- newManager defaultManagerSettings
    let houseAccount = "addr-house-account"
        initState = CoinMixerState houseAccount manager conn
    hold <- newEmptyMVar
    _ <- launchTaker hold initState conn
    _ <- launchDoler hold initState conn
    res <- readMVar hold
    case res of
      Left (SomeException e) -> throw e
      Right _ -> return ()
  where
    launchTaker = launchWorker (void . takeIn) 5
    launchDoler = launchWorker doleOutDeposit 10
    launchWorker
      :: Worker -- ^ The worker
      -> Int -- ^ Poll time, in seconds
      -> MVar (Either SomeException a) -- ^ MVar Connector
      -> CoinMixerState -- ^ Initial state
      -> Connection -- ^ Redis connection pool
      -> IO ThreadId
    launchWorker worker n var state conn =
      forkFinally (forever loop) (\e -> putMVar var e)
        where
          loop = run >> sleep >> loop
          run = do
            addrs <- R.runRedis conn getAllDepositAddresses
            mapM_ runFlow addrs

          sleep = liftIO . threadDelay $ n * 1000000
          runFlow addr =
            handleErrors =<< runCoinMixer state (worker addr)

    -- | Get all the deposit addresses from the DB and launch a worker
    -- to poll it.
    getAllDepositAddresses :: R.Redis [Text]
    getAllDepositAddresses = do
      ks <- R.smembers "deposit"
      let keys = either fatalErr id ks
      return $ fmap decodeUtf8 keys
      where fatalErr reply = error $ "Fatal error: failed to get deposit addresses with reply\n\t"
                          ++ show reply

    -- | Error handler. Some errors are fine (ie: NoBalance) but others aren't.
    handleErrors :: Either CoinMixerError a -> IO ()
    handleErrors (Right _) = return ()
    handleErrors (Left e) =
      case e of
        RedisError re -> error $ "Got an error from Redis:\n\t" ++ show re
        NoBalance -> return ()
        InsufficientFunds msg ->
          error $ "Failed to transfer funds:\n\t" ++ unpack msg
        DecodeError msg ->
          error $ "Failed to decode type from Redis\n\t" ++ msg


