{-# LANGUAGE OverloadedStrings #-}
module JobCoin.Client where

import Data.Aeson (eitherDecode)
import Data.Monoid ((<>))
import Data.Text (Text, unpack)
import Data.Text.Encoding (encodeUtf8)
import JobCoin.Data
import Network.HTTP.Client
import Network.HTTP.Types.Status (statusCode)
import Network.HTTP.Client.MultipartFormData

import qualified Data.ByteString.Char8 as BC

apiRoot :: Text
apiRoot = "http://jobcoin.projecticeland.net/nymphaeaceous/api"

-- | Get the balance and all the transactions from or to some given address
getAddressInfo :: Manager -> Addr -> IO AddressInfo
getAddressInfo manager addr = do
  req <- parseUrl . unpack $ apiRoot <> "/addresses/" <> addr
  response <- httpLbs req manager
  either error return $ eitherDecode (responseBody response)

-- | Get all transactions in the JobCoin network
getAllTransactions :: Manager -> IO [Transaction]
getAllTransactions manager = do
  req <- parseUrl . unpack $ apiRoot <> "/transactions"
  response <- httpLbs req manager
  either error return $ eitherDecode (responseBody response)

-- | Transfer funds from one address to another.
-- Return @True@ if successful, @False@ if it failed due to insufficient funds,
-- and an exception for all other errors
transferBTC :: Manager -> Addr -> Addr -> Double -> IO Bool
transferBTC manager from to amt = do
  req <- parseUrl . unpack $ apiRoot <> "/transactions"
  req2 <- formDataBody parts req
  response <- httpLbs req2 manager
  let resCode = statusCode $ responseStatus response
  case resCode of
    200 -> return True
    422 -> return False
    c   -> error $ "Unexcepted status code" ++ show c
  where
    parts = [ partBS "fromAddress" $ encodeUtf8 from
            , partBS "toAddress" $ encodeUtf8 to
            , partBS "amount" . BC.pack $ show amt
            ]
