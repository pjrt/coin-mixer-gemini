{-# LANGUAGE OverloadedStrings #-}
module JobCoin.Data where

import Data.Aeson
import Data.Aeson.Types (Parser)
import Data.Time (UTCTime)
import Data.Text (Text)
import Text.Read (readMaybe)

-- | And Address identifier
type Addr = Text

data AddressInfo = AddressInfo {
                     addrBalance      :: Double
                   , addrTransactions :: [Transaction]
                   } deriving Show

instance FromJSON AddressInfo where
  parseJSON = withObject "AddressInfo" $ \o ->
                AddressInfo <$> (o .: "balance" >>= asNumber)
                            <*> o .: "transactions"


data Transaction = Transaction {
                     tranAmount       :: Double
                   , transFromAddress :: Maybe Addr
                   , transToAddres    :: Addr
                   , transTimestamp   :: UTCTime
                   } deriving Show

instance FromJSON Transaction where
  parseJSON = withObject "Transaction" $ \o ->
                Transaction <$> (o .: "amount" >>= asNumber)
                            <*> o .:? "fromAddress"
                            <*> o .: "toAddress"
                            <*> o .: "timestamp"

asNumber :: String -> Parser Double
asNumber txt = maybe (fail "Expected a double") return $ readMaybe txt
