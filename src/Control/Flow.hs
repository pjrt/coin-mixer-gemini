{-# LANGUAGE OverloadedStrings, FlexibleContexts, MultiWayIf #-}
{-| A set of functions that define the flow of money from a deposit to the
 - withdraw addresses
-}
module Control.Flow(flow, takeIn, doleOutDeposit) where


import Control.Monad (void)
import Control.Monad.Except (throwError)
import Control.Monad.Reader (liftIO, unless)
import Data.CoinMixer (CoinMixer)
import Data.ByteString (ByteString)
import Data.Monoid ((<>))
import Data.Text (Text, pack, unpack)
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
import JobCoin (Addr, AddressInfo(..), getAddressInfo, transferBTC)
import Text.Read (readEither)

import qualified Control.Monad.Logger as L
import qualified Data.CoinMixer as CM
import qualified Data.Error as E
import qualified Database.Redis as R


-- | Given a deposit address, take all the money from it, deposit it in the
-- house account, and then dole out to the deposit's withdraw addresses.
flow :: Addr -> CoinMixer ()
flow dAddr = do
  balance <- takeIn dAddr
  L.logInfoN $ "Transferred " <> showT balance <> " from " <> dAddr
  withdrawAddrs <- getWithdrawAddresses dAddr
  doleOut balance withdrawAddrs
  L.logInfoN $ "Done with flow for " <> dAddr

getWithdrawAddresses :: Addr -> CoinMixer [Addr]
getWithdrawAddresses dAddr = do
  keys <- CM.liftReply (R.smembers (encodeUtf8 dAddr))
  return $ map decodeUtf8 keys


type Balance = Double

-- | Given an address, take all of its balance and transfer it to the house
-- account. Place the balance under the `out:<address>` keys. Return the
-- balance.
takeIn :: Addr -> CoinMixer Balance
takeIn depositAddr = do
    addrDetailsM <- pollAddr depositAddr
    balance <- transfer addrDetailsM
    L.logInfoN $ "Transfered " <> showT balance <> " from " <> depositAddr
               <> " to house account"
    return balance
  where
    transfer (addr, AddressInfo balance _) = do
      isSuccess <- insertToBigHouse addr balance
      if isSuccess
        then let insertAction = R.incrbyfloat (outKey addr) balance
             in CM.runRedisAction insertAction >> return balance
        else throwError $ E.InsufficientFunds $ addr <> " -> " <> "house $" <> showT balance

    insertToBigHouse addr amt = do
      houseAcct <- CM.getHouseAccount
      manager <- CM.getManager
      liftIO $ transferBTC manager addr houseAcct amt

    pollAddr :: Addr -> CoinMixer (Addr, AddressInfo)
    pollAddr addr = do
      manager <- CM.getManager
      mAddr <- liftIO $ getAddressInfo manager addr
      let balance = addrBalance mAddr
      if balance == 0
          then
            let msg = addr <> " has no balance. Skipping..."
            in L.logDebugN msg >> throwError E.NoBalance
          else return (addr, mAddr) -- Less than zero is impossible


doleOutDeposit :: Addr -> CoinMixer ()
doleOutDeposit addr = do
    balanceRemainingBS <- CM.liftReply (R.get (outKey addr))
    balanceRemaining <- maybe (return 0) decodeSci balanceRemainingBS
    wAddrs <- getWithdrawAddresses addr
    doleOutBalance <- if | balanceRemaining == 0 -> throwError E.NoBalance
                         | balanceRemaining < 10 -> return balanceRemaining
                         | otherwise -> return $ balanceRemaining / 4
    doleOut doleOutBalance wAddrs
    void $ CM.liftReply $ decrementBalance doleOutBalance
  where
    decrementBalance amt =
      R.incrbyfloat (outKey addr) (negate amt)
    decodeSci = either (throwError . E.DecodeError) return . readEither
                                                  . unpack . decodeUtf8


-- | Give a balance and a list of addresses, dole out the balance evenly to
-- from the house address to each address.
doleOut :: Balance -> [Addr] -> CoinMixer ()
doleOut balance addrs = do
    let parts = balance / fromIntegral (length addrs)
    L.logInfoN $ "Dole out " <> showT balance <> " to "
               <> showT (length addrs) <> " addresses. " <> showT parts <> " each"
    mapM_ (transferFromHouse parts) addrs
  where
    transferFromHouse amt addr = do
      houseAddr <- CM.getHouseAccount
      manager <- CM.getManager
      errIfFalse . liftIO $ transferBTC manager houseAddr addr amt
      where
        errIfFalse act = do
          isSuccess <- act
          unless isSuccess $
            throwError $ E.InsufficientFunds $ "house -> " <> addr <> " " <> showT amt

-- Utils

showT :: Show a => a -> Text
showT = pack . show

-- | The 'out' key, used to keeping track of how much money we have for which
-- deposit address
outKey :: Text -> ByteString
outKey key =
  let v = "out:" <> key
  in encodeUtf8 v
