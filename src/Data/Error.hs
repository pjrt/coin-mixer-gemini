module Data.Error where

import Data.Text (Text)
import Database.Redis (Reply)

data CoinMixerError = RedisError Reply -- ^ A generic Redis error
                    | NoBalance -- ^ No funds in the deposit account. Skip it.
                    | InsufficientFunds Text -- ^ Failed to transfer funds due to lack of them
                    | DecodeError String -- ^ Some sort of decoding error when reading from Redis
