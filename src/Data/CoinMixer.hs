{-# LANGUAGE FlexibleContexts, RankNTypes #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Data.CoinMixer (
  CoinMixer
, runCoinMixer
, CoinMixerState(CoinMixerState)
, getHouseAccount
, getManager
, getRedisConnection
, runRedisAction
, liftReply
) where

import Control.Monad.Logger (MonadLogger, MonadLoggerIO, LoggingT)
import Control.Monad.Reader
import Control.Monad.Except (ExceptT(..), MonadError, runExceptT, throwError)
import Database.Redis (Connection, Redis, Reply, runRedis)
import Data.Error
import Network.HTTP.Client (Manager)

import JobCoin.Data (Addr)

import qualified Control.Monad.Logger as L


-- | State of the app
data CoinMixerState = CoinMixerState {
                        _getHouseAccount :: Addr -- ^ House Account address
                      , _getManager :: Manager -- ^ HTTP manager
                      , _getRedisConn :: Connection -- ^ A Redis connection pool
                      }

-- | Main App type. A stack of Reader onto of IO with Logging
newtype CoinMixer a =
  CoinMixer {
    getCM :: ReaderT CoinMixerState (LoggingT (ExceptT CoinMixerError IO)) a
  } deriving (Functor, Applicative, Monad, MonadIO,
              MonadReader CoinMixerState, MonadLogger, MonadLoggerIO,
              MonadError CoinMixerError)

-- | Run a CoinMixer action using the given state, logging to Stdout
runCoinMixer :: CoinMixerState -> CoinMixer a -> IO (Either CoinMixerError a)
runCoinMixer state =
    runExceptT . L.runStdoutLoggingT
  . flip runReaderT state . getCM

getHouseAccount :: CoinMixer Addr
getHouseAccount = fmap _getHouseAccount ask

getManager :: CoinMixer Manager
getManager = fmap _getManager ask

getRedisConnection :: CoinMixer Connection
getRedisConnection = fmap _getRedisConn ask

-- | Lift a redis action into CoinMixer
runRedisAction :: Redis a -> CoinMixer a
runRedisAction act = do
  conn <- getRedisConnection
  liftIO $ runRedis conn act

-- | Lift a Redis reply into a CoinMixer
liftReply :: Redis (Either Reply a) -> CoinMixer a
liftReply rAct = either (throwError . RedisError) return =<< runRedisAction rAct
