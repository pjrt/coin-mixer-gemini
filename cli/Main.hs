{-# LANGUAGE OverloadedStrings #-}
module Main where

import qualified Data.ByteString.Char8 as C8
import Data.UUID (toString)
import Data.UUID.V4 (nextRandom)
import Database.Redis (connect, defaultConnectInfo, runRedis)
import qualified Database.Redis as R
import Options.Applicative

main :: IO ()
main = do
  withdraws <- execParser opts
  depositAddr <- toString <$> nextRandom
  conn <- connect defaultConnectInfo
  runRedis conn $ do
    let bsAddr = C8.pack depositAddr
    _ <- R.sadd "deposit" [bsAddr]
    _ <- R.sadd bsAddr $ map C8.pack withdraws
    return ()
  putStrLn depositAddr -- return the deposit address to the user
  where
    opts = info (helper <*> parseWithdrawls)
                (fullDesc <> progDesc "A cli for the JobCoin Mixer")


parseWithdrawls :: Parser [String]
parseWithdrawls =
  some $ argument str (metavar "WITHDRAWLS")
